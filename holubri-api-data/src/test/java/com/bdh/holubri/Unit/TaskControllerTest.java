package com.bdh.holubri.Unit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.bdh.holubri.backend.data.entity.Task;

public class TaskControllerTest extends AbstractTest {

	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	/**
	 * Get all tasks
	 */
	@Test
	@WithMockUser(username = "test", password = "pwd", roles = "USER")
	public void getAllTasks() throws Exception {
		String uriString = "/api/task";
		MvcResult mvcResult = mvc
				.perform(MockMvcRequestBuilders.get(uriString).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);

		String content = mvcResult.getResponse().getContentAsString();
		Task[] taskList = super.mapFromJson(content, Task[].class);
		assertTrue(taskList.length > 0);
	}
}
