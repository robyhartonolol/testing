package com.bdh.holubri.Unit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.bdh.holubri.backend.data.Role;
import com.bdh.holubri.backend.data.entity.Task;
import com.bdh.holubri.backend.data.entity.User;

public class UserControllerTest extends AbstractTest {

	@Override
	@Before
	public void setUp() {
		super.setUp();

	}

	/**
	 * Get all users test
	 * 
	 * @throws Exception
	 */
	@Test
	public void getAllUsers() throws Exception {
		String uriString = "/api/user/all";
		MvcResult mvcResult = mvc
				.perform(MockMvcRequestBuilders.get(uriString).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);

		String content = mvcResult.getResponse().getContentAsString();
		User[] userList = super.mapFromJson(content, User[].class);
		assertTrue(userList.length > 0);
	}

	/**
	 * Create / register a user
	 * 
	 * @throws Exception
	 */
	@Test
	public void createUser() throws Exception {
		String uriString = "/api/user/registration";

		User user = new User();
		user.setId(Long.valueOf("322"));
		user.setUsername("test");
		user.setName("John Doe");
		user.setPassword("123");
		user.setRole(Role.USER);

		String inputJson = super.mapToJson(user);
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.post(uriString).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(201, status);

	}

	/**
	 * Delete a user based on id
	 * 
	 * @throws Exception
	 */
	@Test
	public void deleteUser() throws Exception {
		String uriString = "/api/user/1";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uriString)).andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);

	}

	@Test
	public void updateUser() throws Exception {
		String uriString = "/api/user/3";

		User updatedUser = new User();
		updatedUser.setName("Update Test");

		String inputJson = super.mapToJson(updatedUser);
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.put(uriString).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}

}
