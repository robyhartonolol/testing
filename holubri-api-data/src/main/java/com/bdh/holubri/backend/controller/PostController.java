package com.bdh.holubri.backend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bdh.holubri.backend.data.entity.Post;
import com.bdh.holubri.backend.implementation.PostServiceImp;

@RestController
public class PostController {
	@Autowired
	private PostServiceImp postServiceImp;

	@RequestMapping("/api/post")
	public List<Post> getAllPosts() {
		return postServiceImp.getAllPosts();
	}

	@RequestMapping("/api/post/{id}")
	public Post getPost(@PathVariable Integer id) {
		return postServiceImp.getPost(id);
	}

	@RequestMapping(method = RequestMethod.POST, value = "api/post")
	public void addPost(@RequestBody Post post) {
		postServiceImp.addPost(post);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "api/post/{id}")
	public void updatePost(@RequestBody Post post, @PathVariable Integer id) {
		postServiceImp.updatePost(id, post);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "api/post/{id}")
	public void deletePost(@PathVariable Integer id) {
		postServiceImp.deletePost(id);
	}
}
