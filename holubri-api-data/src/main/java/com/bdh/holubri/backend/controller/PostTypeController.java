package com.bdh.holubri.backend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bdh.holubri.backend.data.entity.PostType;
import com.bdh.holubri.backend.implementation.PostTypeServiceImp;



@RestController
public class PostTypeController {
	@Autowired
	private PostTypeServiceImp postTypeServiceImp;

	@RequestMapping("/api/posttype")
	public List<PostType> getAllPostTypes() {
		return postTypeServiceImp.getAllPostTypes();
	}

	@RequestMapping("/api/posttype/{id}")
	public PostType getPostType(@PathVariable Integer id) {
		return postTypeServiceImp.getPostType(id);
	}

	@RequestMapping(method = RequestMethod.POST, value = "api/posttype")
	public void addPostType(@RequestBody PostType messageType) {
		postTypeServiceImp.addPostType(messageType);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "api/posttype/{id}")
	public void updatePostType(@RequestBody PostType messageType, @PathVariable Integer id) {
		postTypeServiceImp.updatePostType(id, messageType);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "api/posttype/{id}")
	public void deletePostType(@PathVariable Integer id) {
		postTypeServiceImp.deletePostType(id);
	}
}
