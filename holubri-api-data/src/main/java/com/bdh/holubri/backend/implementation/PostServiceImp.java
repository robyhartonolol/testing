package com.bdh.holubri.backend.implementation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdh.holubri.backend.NullPropertiesUtils;
import com.bdh.holubri.backend.data.entity.Post;
import com.bdh.holubri.backend.data.entity.Station;
import com.bdh.holubri.backend.repositories.PostRepository;
import com.bdh.holubri.backend.service.PostService;

@Service
public class PostServiceImp implements PostService {
	@Autowired
	private PostRepository postRepository;

	@Override
	public List<Post> getAllPosts() {
		List<Post> mPosts = new ArrayList<>();
		postRepository.findAll().forEach(mPosts::add);
		return mPosts;
	}

	@Override
	public Post getPost(Integer id) {
		return postRepository.findById(id).get();
	}

	@Override
	public void addPost(Post post) {
		postRepository.save(post);
	}

	@Override
	public void deletePost(Integer id) {
		postRepository.deleteById(id);
	}

	@Override
	public void updatePost(Integer id, Post post) {

		// Get existed Post from db based on the id
		Post oldPost = postRepository.findById(id).orElse(null);
		// Copy the properties values of posted Post object that is not null to the
		// old
		NullPropertiesUtils.copyNonNullProperties(post, oldPost);
		postRepository.save(oldPost);
	}

}
