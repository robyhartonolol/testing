package com.bdh.holubri.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bdh.holubri.backend.data.entity.PostType;

public interface PostTypeRepository extends JpaRepository<PostType, Integer> {
	

}
