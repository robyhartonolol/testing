package com.bdh.holubri.backend.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bdh.holubri.backend.data.entity.User;

/**
 * Interface for User Repository
 * 
 * @author Roby Hartono
 *
 */
public interface UserRepository extends JpaRepository<User, Long> {
	Optional<User> findByUsername(String username);

	Optional<User> findById(Long id);
	
	@Query("Select u.name from User u where u.id in (:pIdList)")
	List<String> findUserIdList(@Param("pIdList") List<Long> idList);
}
