package com.bdh.holubri.backend.implementation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdh.holubri.backend.NullPropertiesUtils;
import com.bdh.holubri.backend.data.entity.Patient;
import com.bdh.holubri.backend.data.entity.Station;
import com.bdh.holubri.backend.repositories.PatientRepository;
import com.bdh.holubri.backend.service.PatientService;

@Service
public class PatientServiceImp implements PatientService {

	@Autowired
	private PatientRepository patientRepository;

	public List<Patient> getAllPatients() {
		List<Patient> mPatients = new ArrayList<>();
		patientRepository.findAll().forEach(mPatients::add);
		return mPatients;
	}

	@Override
	public Patient getPatient(Integer id) {
		return patientRepository.findById(id).get();
	}

	@Override
	public void addPatient(Patient patient) {
		patientRepository.save(patient);
	}

	@Override
	public void deletePatient(Integer id) {
		patientRepository.deleteById(id);
	}

	@Override
	public void updatePatient(Integer id, Patient patient) {

		// Get existed Patient from db based on the id
		Patient oldPatient = patientRepository.findById(id).orElse(null);
		// Copy the properties values of posted Patient object that is not null to the
		// old
		NullPropertiesUtils.copyNonNullProperties(patient, oldPatient);
		patientRepository.save(oldPatient);
	}

}
