package com.bdh.holubri.backend.data.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;

/**
 * Model class for Task
 * 
 * @author Roby Hartono
 *
 */
@Data
@Entity
@Table(name = "task")
public class Task implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	// @Column(name = "task_id")
	private Integer id;

	// @JsonManagedReference
	@ManyToOne
	@JoinColumn(name = "tasktype_id")
	private TaskType taskType;

	// @JsonManagedReference
	@ManyToOne
	@JoinColumn(name = "start_location_id")
	private Location startLocation;

	// @JsonManagedReference
	@ManyToOne
	@JoinColumn(name = "end_location_id")
	private Location endLocation;

	// @JsonManagedReference
	@ManyToOne
	@JoinColumn(name = "station_id")
	private Station station;

	// @JsonManagedReference
	@ManyToOne
	@JoinColumn(name = "patient_id")
	private Patient patient;

	@ManyToOne
	@JoinTable(name = "user_task", joinColumns = {
			@JoinColumn(name = "task_id", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "user_id", referencedColumnName = "id") })
	// @JsonManagedReference
	// @JsonBackReference
	private User owner;

	@Column(name = "plan_date_time", columnDefinition = "DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date planDateTime;

	@Column(name = "begin_date_time", columnDefinition = "DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date beginDateTime;

	@Column(name = "end_date_time", columnDefinition = "DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDateTime;

	@Column(name = "handler_name")
	private String behandlerName;

	@Column(name = "hub_list_bool")
	private Boolean hubListBoolean;

	@Column(name = "hub_mark_bool")
	private Boolean hubMarkBoolean;

	@Column(name = "hub_completed")
	private Boolean hubCompleted;

	@Column(name = "elevator")
	private String elevatorString;

	@Column(name = "bridge_court")
	private String bridgeCourtString;

	@Column(name = "movement_profile")
	private String movementProfileString;

	@Column(name = "ready_status")
	private Boolean readyStatus;
}
