package com.bdh.holubri.backend.service;

import java.util.List;

import com.bdh.holubri.backend.data.entity.Location;

/**
 * Service interface for Location
 * 
 * @author Roby Hartono
 *
 */
public interface LocationService {
	// Get all locations
	List<Location> getAllLocations();

	// Get a location
	Location getLocation(Integer id);

	// Add task type
	void addLocation(Location location);

	// Delete task type
	void deleteLocation(Integer id);

	// Update task type
	void updateLocation(Integer id, Location location);
}
