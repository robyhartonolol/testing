package com.bdh.holubri.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bdh.holubri.backend.data.entity.TaskType;

/**
 * Interface for TaskType repository
 * 
 * @author Roby Hartono
 *
 */
public interface TaskTypeRepository extends JpaRepository<TaskType, Long> {
}
