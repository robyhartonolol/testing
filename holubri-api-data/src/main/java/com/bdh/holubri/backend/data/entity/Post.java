package com.bdh.holubri.backend.data.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;

@Data
@Entity
@Table(name = "post")
public class Post implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "post_type_id")
	private PostType postType;

	// Creator of the post
	@ManyToOne
	@JoinColumn(name = "creator_id")
	//@JsonBackReference
	//@JsonManagedReference
	private User creator;

	@Column(name = "created_at")
	private Date createdAtTime;
	
	@Column(name = "updated_at")
	private Date updatedAtTime;
}
