package com.bdh.holubri.backend.data;

/**
 * List of possible roles 
 * 
 * @author Roby Hartono
 *
 */
public enum Role {
	USER, ADMIN
}
