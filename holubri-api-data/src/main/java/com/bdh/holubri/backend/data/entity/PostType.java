package com.bdh.holubri.backend.data.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.bdh.holubri.backend.data.Role;
import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;
import lombok.experimental.PackagePrivate;

@Data
@Entity
@Table(name = "post_type")
public class PostType implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "post_name")
	private String messageName; 
		
	// in User folder
	@Enumerated(EnumType.STRING)
	@Column(name = "post_role")
	private Role postRole; 
	
	@JsonBackReference
	@OneToMany(mappedBy = "postType", cascade = CascadeType.ALL, orphanRemoval = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private List<Post> post;
}
