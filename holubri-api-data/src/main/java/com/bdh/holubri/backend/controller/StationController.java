package com.bdh.holubri.backend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bdh.holubri.backend.data.entity.Station;
import com.bdh.holubri.backend.implementation.StationServiceImp;

@RestController
public class StationController {
	@Autowired 
	private StationServiceImp stationServiceImp; 
	
	@RequestMapping("/api/station")
	public List<Station> getAllStations() {
		return stationServiceImp.getAllStations(); 
	}
	
	@RequestMapping("/api/station/{id}")
	public Station getStation(@PathVariable Integer id) {
		return stationServiceImp.getStation(id); 
	}
	
	@RequestMapping(method=RequestMethod.POST, value="api/station")
	public void addStation(@RequestBody Station station) {
		stationServiceImp.addStation(station); 
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="api/station/{id}")
	public void updateStation(@RequestBody Station station, @PathVariable Integer id) {
		stationServiceImp.updateStation(id, station);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="api/station/{id}")
	public void deleteStation(@PathVariable Integer id) {
		stationServiceImp.deleteStation(id); 
	}

}
