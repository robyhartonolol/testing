package com.bdh.holubri.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bdh.holubri.backend.data.entity.Station;

public interface StationRepository extends JpaRepository<Station, Integer> {
	
}
