package com.bdh.holubri.backend.implementation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdh.holubri.backend.NullPropertiesUtils;
import com.bdh.holubri.backend.data.entity.TaskType;
import com.bdh.holubri.backend.data.entity.User;
import com.bdh.holubri.backend.repositories.TaskTypeRepository;
import com.bdh.holubri.backend.service.TaskTypeService;

/**
 * Implementation class of interface TaskTypeService
 * 
 * @author Roby Hartono
 *
 */
@Service
public class TaskTypeServiceImp implements TaskTypeService {

	@Autowired
	private TaskTypeRepository taskTypeRepository;
	
	@Autowired
	private NullPropertiesUtils helperClass; 

	/**
	 * Get all task types
	 * 
	 * @return List of all task types
	 */
	@Override
	public List<TaskType> getAllTaskTypes() {
		List<TaskType> mTaskTypes = new ArrayList<>();
		taskTypeRepository.findAll().forEach(mTaskTypes::add);
		return mTaskTypes;
	}

	/**
	 * Get a task type based on id
	 * 
	 * @return a task type
	 */
	@Override
	public TaskType getTaskType(Long id) {
		return taskTypeRepository.findById(id).get();
	}

	/**
	 * Create / Add a task type
	 * 
	 */
	@Override
	public void addTaskType(TaskType taskType) {
		taskTypeRepository.save(taskType);

	}

	/**
	 * Delete a task type based on id
	 * 
	 */
	@Override
	public void deleteTaskType(Long id) {
		taskTypeRepository.deleteById(id);

	}

	/**
	 * Update a task type
	 * 
	 */
	@Override
	public void updateTaskType(Long id, TaskType taskType) {
		// Get existed user from db based on the id
		TaskType oldTaskType = taskTypeRepository.findById(id).orElse(null);
		// Copy the properties values of posted User object that is not null to the old
		// one
		NullPropertiesUtils.copyNonNullProperties(taskType, oldTaskType);
		taskTypeRepository.save(oldTaskType);

	}

}
