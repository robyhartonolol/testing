package com.bdh.holubri.backend.service;

import java.util.List;

import com.bdh.holubri.backend.data.entity.Post;


public interface PostService {
	// Get all posts
	List<Post> getAllPosts();

	// Get a post
	Post getPost(Integer id);

	// Add message
	void addPost(Post post);

	// Delete message
	void deletePost(Integer id);

	// Update message
	void updatePost(Integer id, Post post);
}
