package com.bdh.holubri.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bdh.holubri.backend.data.entity.Patient;

public interface PatientRepository extends JpaRepository<Patient, Integer> {
}
