package com.bdh.holubri.backend.implementation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdh.holubri.backend.NullPropertiesUtils;
import com.bdh.holubri.backend.data.entity.PostType;
import com.bdh.holubri.backend.data.entity.Station;
import com.bdh.holubri.backend.repositories.PostTypeRepository;
import com.bdh.holubri.backend.service.PostTypeService;

@Service
public class PostTypeServiceImp implements PostTypeService {

	@Autowired
	private PostTypeRepository postTypeRepository;

	@Override
	public List<PostType> getAllPostTypes() {
		List<PostType> mPostTypes = new ArrayList<>();
		postTypeRepository.findAll().forEach(mPostTypes::add);
		return mPostTypes;
	}

	@Override
	public PostType getPostType(Integer id) {
		return postTypeRepository.findById(id).get();
	}

	@Override
	public void addPostType(PostType postType) {
		postTypeRepository.save(postType);
	}

	@Override
	public void deletePostType(Integer id) {
		postTypeRepository.deleteById(id);

	}

	@Override
	public void updatePostType(Integer id, PostType postType) {
		// Get existed PostType from db based on the id
		PostType oldPostType = postTypeRepository.findById(id).orElse(null);
		// Copy the properties values of posted PostType object that is not null to the
		// old
		NullPropertiesUtils.copyNonNullProperties(postType, oldPostType);
		postTypeRepository.save(oldPostType);
	}

}
