package com.bdh.holubri.backend.service;

import java.util.List;

import com.bdh.holubri.backend.data.entity.Station;


public interface StationService {
	// Get all stations
    List<Station> getAllStations();
    
    // Get a station 
    Station getStation(Integer id); 
    
    // Add station
    void addStation(Station station);
    
    // Delete station
    void deleteStation(Integer id); 
    
    // Update station
    void updateStation(Integer id, Station station); 
}
