package com.bdh.holubri.backend.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.bdh.holubri.backend.data.entity.Task;

/**
 * Interface for Task Repository
 * 
 * @author Roby Hartono
 *
 */
public interface TaskRepository extends JpaRepository<Task, Integer> {
	// find all data records from today
	// @Query("SELECT * FROM `task' WHERE DATE(`begin_date_time`) = CURDATE()",
	// nativeQuery = true)
	@Query(value = "SELECT t from Task t where DATE(t.beginDateTime) = CURRENT_DATE")
	List<Task> findAllByDate();
}
