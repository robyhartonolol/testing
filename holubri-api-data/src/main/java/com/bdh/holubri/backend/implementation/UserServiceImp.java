package com.bdh.holubri.backend.implementation;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.bdh.holubri.backend.NullPropertiesUtils;
import com.bdh.holubri.backend.data.entity.User;
import com.bdh.holubri.backend.repositories.UserRepository;
import com.bdh.holubri.backend.service.UserService;

/**
 * Implementation class of UserService interface
 * 
 * @author Roby Hartono
 *
 */
@Service
@Transactional
public class UserServiceImp implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	/**
	 * Create / Update user based on the input
	 * 
	 * @param user object "User" to update / create the User
	 * @return created / updated user
	 */
	@Override
	public User save(User user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		userRepository.save(user);
		return user;
	}

	/**
	 * Find user based on the id
	 * 
	 * @param id user id
	 * @return User with the given id or null if not found
	 */
	@Override
	public User findById(Long id) {
		return userRepository.findById(id).orElse(null);
	}

	/**
	 * Find user based on the username
	 * 
	 * @param username String username
	 * @return User with the given username or null if not found
	 */
	@Override
	public User findByUsername(String username) {
		return userRepository.findByUsername(username).orElse(null);
	}

	/**
	 * Find user based on the list of user id
	 * 
	 * @param idList list of user id
	 * @return List of string of all users
	 */
	@Override
	public List<String> findUsers(List<Long> idList) {
		return this.userRepository.findUserIdList(idList);
	}

	/**
	 * Find all users return List of all Users
	 */
	@Override
	public List<User> findAllUsers() {
		return this.userRepository.findAll();
	}

	/**
	 * Delete user based on id
	 */
	@Override
	public void deleteUser(Long id) {
		userRepository.deleteById(id);
	}

	/**
	 * Update user based on id
	 */
	@Override
	public void updateUser(Long id, User user) {
		// Get existed user from db based on the id
		User oldUser = userRepository.findById(id).orElse(null);
		// Copy the properties values of posted User object that is not null to the old
		// one
		NullPropertiesUtils.copyNonNullProperties(user, oldUser);
		userRepository.save(oldUser);
	}
}
