package com.bdh.holubri.backend.service;

import java.util.List;

import com.bdh.holubri.backend.data.entity.Task;
import com.bdh.holubri.backend.data.entity.User;

/**
 * Interface for User Service
 * 
 * @author Roby Hartono
 *
 */
public interface UserService {
	// Update / Create User
	User save(User user);

	// Find user by username
	User findByUsername(String username);
	
	// Find user by id
	User findById(Long id);

	// List of user based on list of id
	List<String> findUsers(List<Long> idList);

	// Get all users
	List<User> findAllUsers();

	// Delete user
	void deleteUser(Long id);

	// Update user
	void updateUser(Long id, User user);
}
