package com.bdh.holubri.backend.service;

import java.util.List;

import com.bdh.holubri.backend.data.entity.Patient;

public interface PatientService {
	// Get all patients
    List<Patient> getAllPatients();
    
    // Get a patient
    Patient getPatient(Integer id); 
    
    // Add patient
    void addPatient(Patient patient);
    
    // Delete patient
    void deletePatient(Integer id); 
    
    // Update patient
    void updatePatient(Integer id, Patient patient); 
}
