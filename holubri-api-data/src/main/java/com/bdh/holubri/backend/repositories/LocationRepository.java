package com.bdh.holubri.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bdh.holubri.backend.data.entity.Location;

/**
 * Repository interface for Location
 * 
 * @author Roby Hartono
 *
 */
public interface LocationRepository extends JpaRepository<Location, Integer> {

}
