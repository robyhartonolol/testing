package com.bdh.holubri.backend.data.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.bdh.holubri.backend.data.Role;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;

/**
 * Model class for User
 * 
 * @author Roby Hartono
 *
 */
@Data
@Entity
@Table(name = "user")
public class User implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	// @Column(name ="user_id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "username")
	private String username;

	@Column(name = "password")
	private String password;

	@Column(name = "login_at")
	private Date loginAtTime;

	@Column(name = "updated_at")
	private Date updatedAtTime;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "user_task", joinColumns = {
			@JoinColumn(name = "user_id", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "task_id", referencedColumnName = "id") })
	// @JsonBackReference
	@JsonIgnore
	private List<Task> tasks;

	// @JsonBackReference
	@OneToMany(mappedBy = "creator", cascade = CascadeType.ALL)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	private List<Post> posts;

	@Enumerated(EnumType.STRING)
	@Column(name = "role")
	private Role role;
}
