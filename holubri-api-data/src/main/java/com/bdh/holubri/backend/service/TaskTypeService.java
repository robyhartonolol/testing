package com.bdh.holubri.backend.service;

import java.util.List;

import com.bdh.holubri.backend.data.entity.TaskType;

/**
 * Interface for TaskType Service
 * 
 * @author Roby Hartono
 *
 */
public interface TaskTypeService {
	    // Get all task types 
	    List<TaskType> getAllTaskTypes();
	    
	    // Get a task type
	    TaskType getTaskType(Long id); 
	    
	    // Add task type
	    void addTaskType(TaskType taskType);
	    
	    // Delete task type
	    void deleteTaskType(Long id); 
	    
	    // Update task type
	    void updateTaskType(Long id, TaskType taskType); 
}
