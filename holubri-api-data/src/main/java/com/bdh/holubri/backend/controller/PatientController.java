package com.bdh.holubri.backend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bdh.holubri.backend.data.entity.Patient;
import com.bdh.holubri.backend.implementation.PatientServiceImp;


@RestController
public class PatientController {
	@Autowired
	private PatientServiceImp patientServiceImp;

	@RequestMapping("/api/patient")
	public List<Patient> getAllPatients() {
		return patientServiceImp.getAllPatients();
	}

	@RequestMapping("/api/patient/{id}")
	public Patient getPatient(@PathVariable Integer id) {
		return patientServiceImp.getPatient(id);
	}

	@RequestMapping(method = RequestMethod.POST, value = "api/patient")
	public void addPatient(@RequestBody Patient patient) {
		patientServiceImp.addPatient(patient);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "api/patient/{id}")
	public void updatePatient(@RequestBody Patient station, @PathVariable Integer id) {
		patientServiceImp.updatePatient(id, station);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "api/patient/{id}")
	public void deletePatient(@PathVariable Integer id) {
		patientServiceImp.deletePatient(id);
	}

}
