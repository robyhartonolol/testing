package com.bdh.holubri.backend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bdh.holubri.backend.data.entity.TaskType;
import com.bdh.holubri.backend.implementation.TaskTypeServiceImp;

/**
 * Controller class for TaskType
 * 
 * @author Roby Hartono
 *
 */
@RestController
public class TaskTypeController {
	@Autowired
	TaskTypeServiceImp taskTypeServiceImp; 
	
	@RequestMapping("/api/tasktype")
	public List<TaskType> getAllTaskTypes() {
		return taskTypeServiceImp.getAllTaskTypes(); 
	}
	
	@RequestMapping("/api/tasktype/{id}")
	public TaskType getTaskType(@PathVariable Long id) {
		return taskTypeServiceImp.getTaskType(id); 
	}
	
	@RequestMapping(method=RequestMethod.POST, value="api/tasktype")
	public void addTaskType(@RequestBody TaskType taskType) {
		taskTypeServiceImp.addTaskType(taskType); 
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="api/tasktype/{id}")
	public void updateTaskType(@RequestBody TaskType taskType, @PathVariable Long id) {
		taskTypeServiceImp.updateTaskType(id, taskType);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="api/tasktype/{id}")
	public void deleteTaskType(@PathVariable Long id) {
		taskTypeServiceImp.deleteTaskType(id); 
	}
}
