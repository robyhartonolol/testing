package com.bdh.holubri.backend.implementation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdh.holubri.backend.NullPropertiesUtils;
import com.bdh.holubri.backend.data.entity.Task;
import com.bdh.holubri.backend.repositories.TaskRepository;
import com.bdh.holubri.backend.service.TaskService;

/**
 * Implementation class of interface TaskService
 * 
 * @author Roby Hartono
 *
 */
@Service
public class TaskServiceImp implements TaskService {

	@Autowired
	private TaskRepository taskRepository;

	@Override
	public List<Task> getAllTasks() {
		List<Task> mTasks = new ArrayList<>();
		taskRepository.findAll().forEach(mTasks::add);
		return mTasks;
	}

	@Override
	public List<Task> getAllTasksByDate() {
		List<Task> mTasks = new ArrayList<>();
		taskRepository.findAllByDate().forEach(mTasks::add);
		return mTasks;
	}

	@Override
	public Task getTask(Integer id) {
		return taskRepository.findById(id).get();
	}

	@Override
	public void addTask(Task task) {
		taskRepository.save(task);

	}

	@Override
	public void deleteTask(Integer id) {
		taskRepository.deleteById(id);

	}

	@Override
	public void updateTask(Integer id, Task task) {
		// Get existed task from db based on the id
		Task oldTask = taskRepository.findById(id).orElse(null);

		// if a task already owned then don't update the record
		if (oldTask.getOwner() != null) {

		}
		// Copy the properties values of posted Task object that is not null to the old
		// one
		else {
			NullPropertiesUtils.copyNonNullProperties(task, oldTask);
			taskRepository.save(oldTask);
		}
	}
}
