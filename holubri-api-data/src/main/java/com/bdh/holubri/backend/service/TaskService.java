package com.bdh.holubri.backend.service;

import java.util.List;

import com.bdh.holubri.backend.data.entity.Task;

/**
 * Interface for Task Service
 * 
 * @author Roby Hartono
 *
 */
public interface TaskService {
	// Get all Tasks
	List<Task> getAllTasks();

	// Get all Tasks from today
	List<Task> getAllTasksByDate();

	// Get a task
	Task getTask(Integer id);

	// Add task type
	void addTask(Task task);

	// Delete task type
	void deleteTask(Integer id);

	// Update task type
	void updateTask(Integer id, Task task);
}
