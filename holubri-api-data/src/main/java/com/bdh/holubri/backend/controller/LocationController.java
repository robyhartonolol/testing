package com.bdh.holubri.backend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bdh.holubri.backend.data.entity.Location;
import com.bdh.holubri.backend.implementation.LocationServiceImp;

/**
 * Rest Controller class for Location
 * 
 * @author Roby Hartono
 *
 */
@RestController
public class LocationController {
	@Autowired
	private LocationServiceImp locationServiceImp;

	@RequestMapping("/api/location")
	public List<Location> getAllLocations() {
		return locationServiceImp.getAllLocations();
	}

	@RequestMapping("/api/location/{id}")
	public Location getLocation(@PathVariable Integer id) {
		return locationServiceImp.getLocation(id);
	}

	@RequestMapping(method = RequestMethod.POST, value = "api/location")
	public void addLocation(@RequestBody Location location) {
		locationServiceImp.addLocation(location);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "api/location/{id}")
	public void updateLocation(@RequestBody Location location, @PathVariable Integer id) {
		locationServiceImp.updateLocation(id, location);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "api/location/{id}")
	public void deleteLocation(@PathVariable Integer id) {
		locationServiceImp.deleteLocation(id);
	}

}
