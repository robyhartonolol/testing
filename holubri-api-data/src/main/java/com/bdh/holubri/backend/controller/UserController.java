package com.bdh.holubri.backend.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bdh.holubri.backend.data.Role;
import com.bdh.holubri.backend.data.entity.Task;
import com.bdh.holubri.backend.data.entity.User;
import com.bdh.holubri.backend.implementation.UserServiceImp;

/**
 * Controller class for user
 * 
 * @author Roby Hartono
 *
 */
@RestController
public class UserController {

	@Autowired
	private UserServiceImp userServiceImp;

	/**
	 * Handles Post Request for the registration
	 * 
	 * @param user
	 * @return ResponseEntity a response object to notify either the registration
	 *         successful or not
	 */
	@PostMapping("/api/user/registration")
	public ResponseEntity<?> register(@RequestBody User user) {
		// Check in database if the user with the give username exist or not
		if (userServiceImp.findByUsername(user.getUsername()) != null) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}
		// if Role of posted user is null then set it by default with role as a USER
		if (user.getRole() == null) {
			user.setRole(Role.USER);
		}
		userServiceImp.save(user);
		return new ResponseEntity<>(user, HttpStatus.CREATED);
	}

	/**
	 * Handles Get Request for the login
	 * 
	 * @param principal object needed for authentication process through spring
	 *                  security framework
	 * @return ResponseEntity a response object to notify either the login
	 *         successful or not
	 */
	@GetMapping("/api/user/login")
	public ResponseEntity<?> auth(Principal principal) {
		if (principal == null || principal.getName() == null) {
			return ResponseEntity.ok(principal);
		}
		return new ResponseEntity<>(userServiceImp.findByUsername(principal.getName()), HttpStatus.OK);
	}

	/**
	 * Handles Get Request to get all user object
	 * 
	 * @return ResponseEntity a response object list of all users
	 */
	@GetMapping("/api/user/all")
	public ResponseEntity<?> getAllUsers() {
		return ResponseEntity.ok(userServiceImp.findAllUsers());
	}

	/**
	 * Handles Put Request to update a user
	 * 
	 * @param user
	 * @param id
	 */
	@RequestMapping(method = RequestMethod.PUT, value = "api/user/{id}")
	public void updateUser(@RequestBody User user, @PathVariable Long id) {
		userServiceImp.updateUser(id, user);
	}

	/**
	 * Handles Delete request to delete a user
	 * 
	 * @param id
	 */
	@RequestMapping(method = RequestMethod.DELETE, value = "api/user/{id}")
	public void deleteUser(@PathVariable Long id) {
		userServiceImp.deleteUser(id);
	}
}
