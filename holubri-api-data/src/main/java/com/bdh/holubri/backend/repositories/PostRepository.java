package com.bdh.holubri.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bdh.holubri.backend.data.entity.Post;

public interface PostRepository extends JpaRepository<Post, Integer> {

}
