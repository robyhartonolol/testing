package com.bdh.holubri.backend.service;

import java.util.List;

import com.bdh.holubri.backend.data.entity.PostType;

public interface PostTypeService {
	// Get all message types
	List<PostType> getAllPostTypes();

	// Get a message type
	PostType getPostType(Integer id);

	// Add message type
	void addPostType(PostType postType);

	// Delete message type
	void deletePostType(Integer id);

	// Update message type
	void updatePostType(Integer id, PostType postType);
}
