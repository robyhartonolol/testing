package com.bdh.holubri.backend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bdh.holubri.backend.data.entity.Task;
import com.bdh.holubri.backend.implementation.TaskServiceImp;

/**
 * Controller class for Task
 * 
 * @author Roby Hartono
 *
 */
@RestController
public class TaskController {

	@Autowired
	private TaskServiceImp taskServiceImp;

	@RequestMapping("/api/task")
	public List<Task> getAllTasks() {
		return taskServiceImp.getAllTasks();
	}

	@RequestMapping("/api/task/today")
	public List<Task> getAllTasksToday() {
		return taskServiceImp.getAllTasksByDate();
	}

	@RequestMapping("/api/task/{id}")
	public Task getTask(@PathVariable Integer id) {
		return taskServiceImp.getTask(id);
	}

	@RequestMapping(method = RequestMethod.POST, value = "api/task", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void addTask(@RequestBody Task task) {
		taskServiceImp.addTask(task);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "api/task/{id}")
	public void updateTask(@RequestBody Task task, @PathVariable Integer id) {
		taskServiceImp.updateTask(id, task);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "api/task/{id}")
	public void deleteTask(@PathVariable Integer id) {
		taskServiceImp.deleteTask(id);
	}
}
