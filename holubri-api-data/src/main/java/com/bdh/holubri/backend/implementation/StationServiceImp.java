package com.bdh.holubri.backend.implementation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdh.holubri.backend.NullPropertiesUtils;
import com.bdh.holubri.backend.data.entity.Station;
import com.bdh.holubri.backend.data.entity.TaskType;
import com.bdh.holubri.backend.repositories.StationRepository;
import com.bdh.holubri.backend.service.StationService;

@Service
public class StationServiceImp implements StationService {

	@Autowired
	private StationRepository stationRepository;

	@Override
	public List<Station> getAllStations() {
		List<Station> mStations = new ArrayList<>();
		stationRepository.findAll().forEach(mStations::add);
		return mStations;
	}

	@Override
	public Station getStation(Integer id) {
		return stationRepository.findById(id).get();
	}

	@Override
	public void addStation(Station station) {
		stationRepository.save(station);

	}

	@Override
	public void deleteStation(Integer id) {
		stationRepository.deleteById(id);
	}

	@Override
	public void updateStation(Integer id, Station station) {

		// Get existed station from db based on the id
		Station oldStation = stationRepository.findById(id).orElse(null);
		// Copy the properties values of posted Station object that is not null to the
		// old
		NullPropertiesUtils.copyNonNullProperties(station, oldStation);
		stationRepository.save(oldStation);
	}

}
