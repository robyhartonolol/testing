package com.bdh.holubri.backend.implementation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdh.holubri.backend.NullPropertiesUtils;
import com.bdh.holubri.backend.data.entity.Location;
import com.bdh.holubri.backend.data.entity.Station;
import com.bdh.holubri.backend.repositories.LocationRepository;
import com.bdh.holubri.backend.service.LocationService;

@Service
public class LocationServiceImp implements LocationService {

	@Autowired
	private LocationRepository locationRepository;

	@Override
	public List<Location> getAllLocations() {
		List<Location> mLocations = new ArrayList<>();
		locationRepository.findAll().forEach(mLocations::add);
		return mLocations;
	}

	@Override
	public Location getLocation(Integer id) {
		return locationRepository.findById(id).get();
	}

	@Override
	public void addLocation(Location location) {
		locationRepository.save(location);

	}

	@Override
	public void deleteLocation(Integer id) {
		locationRepository.deleteById(id);

	}

	@Override
	public void updateLocation(Integer id, Location location) {

		// Get existed Location from db based on the id
		Location oldLocation = locationRepository.findById(id).orElse(null);
		// Copy the properties values of posted Location object that is not null to the
		// old
		NullPropertiesUtils.copyNonNullProperties(location, oldLocation);
		locationRepository.save(oldLocation);
	}

}
