package com.bdh.holubri;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Initializer / Main class
 * @author Roby Hartono
 *
 */
@SpringBootApplication
public class HolubriApplication {

	public static void main(String[] args) {
		SpringApplication.run(HolubriApplication.class, args);
	}
}
