package com.bdh.holubri.app.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Configuration class for the security with Spring Security
 * 
 * @author Roby Hartono
 *
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	private static final String LOGIN_PROCESSING_URL = "/login";
	private static final String LOGIN_FAILURE_URL = "/login";
	private static final String LOGIN_URL = "/login";
	private static final String LOGOUT_SUCCESS_URL = "/login";

	@Autowired
	private UserDetailsService userDetailsService;

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	/**
	 * Configure access to static resources such as javascript, css, etc. 
	 */
	@Override
	public void configure(WebSecurity web) throws Exception {
		// Allow all css and js files from static folder in the resources directory to
		// be accessed without
		// authentication
		web.ignoring().antMatchers("/css/**",

				"/js/**",

				// Vaadin Flow static resources //
				"/VAADIN/**",

				// the standard favicon URI
				"/favicon.ico",

				// the robots exclusion standard
				"/robots.txt",

				// web application manifest //
				"/manifest.webmanifest", "/sw.js", "/offline-page.html",

				// (development mode) static resources //
				"/frontend/**",

				// (development mode) webjars //
				"/webjars/**",

				// (production mode) static resources //
				"/frontend-es5/**", "/frontend-es6/**");
	}

//	@Override
//	protected void configure(HttpSecurity http) throws Exception {
//		// Enable CORS
//		http.cors().and()
//				// starts authorizing configurations
//				.authorizeRequests()
//				// ignoring the guest's urls "
//				.antMatchers("/resources/**", "/api/user/**", "/error", "/login").permitAll()
//				// allow admin urls only accessed by user with ADMIN Role
//				.antMatchers("/admin/**").hasAuthority("ADMIN")
//				// authenticate all remaining URLS
//				.anyRequest().fullyAuthenticated().and()
//				/*
//				 * "/logout" will log the user out by invalidating the HTTP Session, cleaning up
//				 * any {link rememberMe()} authentication that was configured,
//				 */
//				.logout().permitAll().logoutRequestMatcher(new AntPathRequestMatcher("/api/user/logout", "POST")).and()
//				// set login page to login.html if successful then go to admin.html
//				.formLogin().loginPage("/login").defaultSuccessUrl("/success", true).permitAll().and()
//				// enabling the basic authentication
//				// .httpBasic().and()
//				// configuring the session on the server
//				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED).and()
//				// disabling the CSRF - Cross Site Request Forgery
//				.csrf().disable()
//				// Register our CustomRequestCache that saves unauthorized access attempts, so
//				// the user is redirected after login.
//				.requestCache().requestCache(new CustomRequestCache())
//				// Restrict access to our application.
//				.and().authorizeRequests()
//				// Allow all flow internal requests.
//				.requestMatchers(SecurityUtils::isFrameworkInternalRequest).permitAll();
//	}

	/**
	 * Configure HTTP related security here
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// Not using Spring CSRF here to be able to use plain HTML for the login page
		http.csrf().disable() //

				// Register our CustomRequestCache that saves unauthorized access attempts, so
				// the user is redirected after login.
				.requestCache().requestCache(new CustomRequestCache()) //

				// Restrict access to our application.
				.and().authorizeRequests()
				
				// Permit access to these guest URLs  
				.antMatchers("/resources/**", "/api/user/**", "/error", "/login").permitAll()

				// Allow all vaadin flow internal requests.
				.requestMatchers(SecurityUtils::isFrameworkInternalRequest).permitAll() //

				// Allow all requests by logged in users.
				.anyRequest().authenticated() //

				// Configure the login page.
				.and().formLogin().loginPage(LOGIN_URL).defaultSuccessUrl("/success", true).permitAll() //
				.loginProcessingUrl(LOGIN_PROCESSING_URL) //
				//.failureUrl(LOGIN_FAILURE_URL)
				
				//logout" will log the user out by invalidating the HTTP Session, cleaning up
				//any {link rememberMe()} authentication that was configured
				.and().logout().logoutSuccessUrl(LOGOUT_SUCCESS_URL).permitAll().logoutRequestMatcher(new AntPathRequestMatcher("/api/user/logout", "POST"))
				
				// Enable basic authentication for rest end points 
				.and().httpBasic()
				
				// Enable CORS
				.and().cors()
				
				// configuring the session on the server
				.and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED);
		
				
	}

	/**
	 * Configure which authentication should be used by spring security 
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}

	/**
	 * Configure CORS, control access to rest end points from clients 
	 * @return WebMvcConfigurer object for spring security 
	 */
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				// allow access to every end point and from any domains
				// allow get, post, options and put operations 
				registry.addMapping("/**").allowedOrigins("*").allowedMethods("GET", "POST", "OPTIONS", "PUT");
			}
		};
	}
}
