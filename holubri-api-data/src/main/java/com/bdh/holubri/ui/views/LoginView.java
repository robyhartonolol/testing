package com.bdh.holubri.ui.views;

import java.util.Collections;

import com.vaadin.flow.component.login.LoginOverlay;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;


@Route(value = "login")
@PageTitle("Login")
public class LoginView extends VerticalLayout implements BeforeEnterObserver {

	// Create LoginForm component
	private LoginOverlay loginComponent = new LoginOverlay();

	public LoginView() {
		// Set Title
		loginComponent.setTitle("VIKTOR");
		
		// Set Description
		loginComponent.setDescription("Virtuelle Intraklinik Termin Organisator");
		
		// Hide forgot password button
		loginComponent.setForgotPasswordButtonVisible(false);
		
		// Open the overlay
		loginComponent.setOpened(true);

		// Set LoginForm ActionListener
		loginComponent.setAction("/login");

		// Align the LoginForm component at the middle
		setSizeFull();
		
		setHorizontalComponentAlignment(Alignment.CENTER, loginComponent);
		
		// Add the vertical layout to the main view
		add(loginComponent);
	}

	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		// inform the user about an authentication error
		if (!event.getLocation().getQueryParameters().getParameters().getOrDefault("error", Collections.emptyList())
				.isEmpty()) {
			loginComponent.setError(true);
		}
	}
}
