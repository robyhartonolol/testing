package com.bdh.holubri.ui;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bdh.holubri.backend.repositories.TaskRepository;
import com.bdh.holubri.backend.repositories.UserRepository;

/**
 * Controller class for the web application handles HTML Routing in the web
 * application
 * 
 * @author Roby Hartono
 *
 */
@Controller
public class WebController {
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private TaskRepository taskRepository;

//	@RequestMapping(value = "/login", method = RequestMethod.GET)
//	public String login() {
//		return "login";
//	}

	@RequestMapping(value = "/success", method = RequestMethod.GET)
	public void loginPageRedirect(HttpServletRequest request, HttpServletResponse response, Authentication authResult)
			throws IOException, ServletException {
		String roleString = authResult.getAuthorities().toString();

		if (roleString.contains("ADMIN")) {
			response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/admin"));
		} else if (roleString.contains("USER")) {
			response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/home"));
		} else {
			
		}
	}

	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String admin(Model model) {
		model.addAttribute("userList", userRepository.findAll());
		return "admin";
	}

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String home(Model model) {
		model.addAttribute("taskList", taskRepository.findAll());
		return "home";
	}

}
