INSERT INTO task_type (task_name, side_task) VALUES ('Essenswagen Bringen', 0); 
INSERT INTO task_type (task_name, side_task) VALUES ('Essenswagen Rueckfahrt', 0); 
INSERT INTO task_type (task_name, side_task) VALUES ('Patient Bringen', 0); 
INSERT INTO task_type (task_name, side_task) VALUES ('Patient Rueckfahrt', 0);
INSERT INTO task_type (task_name, side_task) VALUES ('Labor', 1);
INSERT INTO task_type (task_name, side_task) VALUES ('Weitere Sonderaufgabe', 1); 


INSERT INTO station (station_name) values ('keine'); 
INSERT INTO station (station_name) values ('Station 1'); 
INSERT INTO station (station_name) values ('Station 2'); 
INSERT INTO station (station_name) values ('Station 3'); 
INSERT INTO station (station_name) values ('Station 4'); 
INSERT INTO station (station_name) values ('Station 5'); 
INSERT INTO station (station_name) values ('Station 6'); 
INSERT INTO station (station_name) values ('Station 7');


INSERT INTO user (name, username, password, role) values ('max', 'maxmusterA', '1234', 'ADMIN');  
INSERT INTO user (name, username, password, role) values ('max', 'maxmusterB', '1234', 'USER'); 
INSERT INTO user (name, username, password, role) values ('max', 'maxmusterC', '1234', 'USER');   
INSERT INTO user (name, username, password, role) values ('max', 'maxmusterD', '1234', 'USER');   
INSERT INTO user (name, username, password, role) values ('max', 'maxmusterE', '1234', 'ADMIN'); 

INSERT INTO patient (patient_name, movement_profile) values ('John','Bett');
INSERT INTO patient (patient_name, movement_profile) values ('Doe','Rollstuhl');
INSERT INTO patient (patient_name, movement_profile) values ('Mark','Rollator');
INSERT INTO patient (patient_name, movement_profile) values ('Max','Rollstuhl');
INSERT INTO patient (patient_name, movement_profile) values ('Tina','Selbstlaeufer');

INSERT INTO post_type (post_name, post_role) values ('Unterbringung der Patienten','USER');
INSERT INTO post_type (post_name, post_role) values ('Bericht erstellen', 'ADMIN'); 

INSERT INTO post (creator_id, post_type_id) values ('1', '1'); 
INSERT INTO post (creator_id, post_type_id) values ('2', '1'); 
INSERT INTO post (creator_id, post_type_id) values ('3', '2'); 
INSERT INTO post (creator_id, post_type_id) values ('4', '1'); 
INSERT INTO post (creator_id, post_type_id) values ('5', '1'); 

INSERT INTO location (location_name) values ('G0'); 
INSERT INTO location (location_name) values ('G1'); 
INSERT INTO location (location_name) values ('B9'); 
INSERT INTO location (location_name) values ('B0'); 
INSERT INTO location (location_name) values ('B1'); 
INSERT INTO location (location_name) values ('B2'); 
INSERT INTO location (location_name) values ('C9'); 
INSERT INTO location (location_name) values ('C0'); 
INSERT INTO location (location_name) values ('C1'); 
INSERT INTO location (location_name) values ('A1'); 
INSERT INTO location (location_name) values ('J0'); 
INSERT INTO location (location_name) values ('J1'); 
INSERT INTO location (location_name) values ('J2'); 
INSERT INTO location (location_name) values ('J3'); 
INSERT INTO location (location_name) values ('F0'); 
INSERT INTO location (location_name) values ('F1'); 
INSERT INTO location (location_name) values ('F2'); 
INSERT INTO location (location_name) values ('F3'); 
INSERT INTO location (location_name) values ('KGS 1'); 
INSERT INTO location (location_name) values ('KGS 2'); 
INSERT INTO location (location_name) values ('KGS 3'); 
INSERT INTO location (location_name) values ('Zentrale'); 
INSERT INTO location (location_name) values ('Labor'); 
INSERT INTO location (location_name) values ('Station 1'); 
INSERT INTO location (location_name) values ('Station 2'); 
INSERT INTO location (location_name) values ('Station 3'); 
INSERT INTO location (location_name) values ('Station 4');
INSERT INTO location (location_name) values ('Station 4 Wartezimmer');  
INSERT INTO location (location_name) values ('Station 5'); 
INSERT INTO location (location_name) values ('Station 6'); 
INSERT INTO location (location_name) values ('Station 7'); 

INSERT INTO `task` (`id`, `begin_date_time`, `handler_name`, `bridge_court`, `elevator`,`movement_profile`, `end_date_time`, `end_location_id`, `hub_list_bool`, `hub_mark_bool`, `hub_completed`, `plan_date_time`, `start_location_id`, `station_id`, `tasktype_id`, `patient_id`) VALUES (NULL, '2019-07-30 06:08:07', 'mark', 'bridge', 'elevator free', 'Rollstuehl', '2019-07-30 06:08:10', '1', b'1', b'0', b'0', '2019-07-30 06:15:00', '29', '3', '4', '1');
INSERT INTO `task` (`id`, `begin_date_time`, `handler_name`, `bridge_court`, `elevator`,`movement_profile`, `end_date_time`, `end_location_id`, `hub_list_bool`, `hub_mark_bool`, `hub_completed`, `plan_date_time`, `start_location_id`, `station_id`, `tasktype_id`, `patient_id`) VALUES (NULL, '2019-07-30 06:08:07', 'mark', 'bridge', 'elevator free','Selbstlauefer', '2019-07-30 06:08:10', '1', b'1', b'0', b'0', '2019-07-30 06:15:00', '29', '3', '3', '2');
INSERT INTO `task` (`id`, `begin_date_time`, `handler_name`, `bridge_court`, `elevator`,`movement_profile`, `end_date_time`, `end_location_id`, `hub_list_bool`, `hub_mark_bool`, `hub_completed`, `plan_date_time`, `start_location_id`, `station_id`, `tasktype_id`, `patient_id`) VALUES (NULL, '2019-07-30 06:08:07', 'mark', 'bridge', 'elevator free', 'Bett','2019-07-30 06:08:10', '1', b'1', b'0', b'0', '2019-07-30 06:15:00', '29', '3', '4', '3');
INSERT INTO `task` (`id`, `begin_date_time`, `handler_name`, `bridge_court`, `elevator`,`movement_profile`, `end_date_time`, `end_location_id`, `hub_list_bool`, `hub_mark_bool`, `hub_completed`, `plan_date_time`, `start_location_id`, `station_id`, `tasktype_id`, `patient_id`) VALUES (NULL, '2019-07-30 06:08:07', 'mark', 'bridge', 'elevator free','Rollator', '2019-07-30 06:08:10', '1', b'1', b'0', b'0', '2019-07-30 06:15:00', '29', '3', '3', '4');
INSERT INTO `task` (`id`, `begin_date_time`, `handler_name`, `bridge_court`, `elevator`,`movement_profile`, `end_date_time`, `end_location_id`, `hub_list_bool`, `hub_mark_bool`, `hub_completed`, `plan_date_time`, `start_location_id`, `station_id`, `tasktype_id`, `patient_id`) VALUES (NULL, '2019-07-30 06:08:07', 'mark', 'bridge', 'elevator free','Selbstlauefer', '2019-07-30 06:08:10', '1', b'1', b'0', b'0', '2019-07-30 06:15:00', '29', '3', '3', '5');

INSERT INTO user_task(user_id, task_id) values ('1', '1');
INSERT INTO user_task(user_id, task_id) values ('1', '2');
INSERT INTO user_task(user_id, task_id) values ('3', '3');
INSERT INTO user_task(user_id, task_id) values ('4', '4');
INSERT INTO user_task(user_id, task_id) values ('5', '5');
