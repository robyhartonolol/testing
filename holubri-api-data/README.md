# HoluBri API 

an application server that connected with the datasource. HoluBri API is using REST(REpresentational State Transfer) interface, so it could be easily consumed using http requests. 

## How to run 

1. Import the directory / project folder to your IDE as a maven project (STS 4, Intelijj)
2. Run HolubriApplication.java as a spring boot application
3. Use your http request client to do get/post requests (Postman, Insomnia, etc.)