import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-mylist",
  templateUrl: "./mylist.page.html",
  styleUrls: ["./mylist.page.scss"]
})
export class MylistPage implements OnInit {
  constructor() {}

  ngOnInit() {}

  /**
   * Refresh the list
   */
  doRefresh(event) {
    console.log("Refresh the list");

    // Get all tasks
    // this.findAllTasks();

    setTimeout(() => {
      console.log("List refreshed");
      event.target.complete();
    }, 2000);
  }
}
