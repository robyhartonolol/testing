import { Component, OnInit } from "@angular/core";
import { AuthService } from "../services/auth.service";
import { User } from "../model/user";
import { Router } from "@angular/router";
import { MenuController, LoadingController } from "@ionic/angular";
import { TaskService } from "../services/task.service";
import { Task } from "../model/task";
import { Patient } from '../model/patient';

@Component({
  selector: "app-list",
  templateUrl: "list.page.html",
  styleUrls: ["list.page.scss"]
})
export class ListPage implements OnInit {
  userList: Array<User>;
  taskList: Array<Task>;
  taskPost: Task = new Task();
  patientList: Array<Patient>;
  errorMessage: string;

  constructor(
    private authService: AuthService,
    private taskService: TaskService,
    private router: Router,
    private menu: MenuController
  ) {}

  ngOnInit() {
    this.menu.enable(true);
    this.findAllUsers();
    this.findAllTasks();
  }

  ionViewWillEnter() {
    this.menu.enable(true);
  }

  findAllUsers() {
    this.authService.findAllUsers().subscribe(data => {
      this.userList = data;
    });
  }

  findAllTasks() {
    this.taskService.findAllTasks().subscribe(data => {
      this.taskList = data;
    });
  }

  markTask(task: Task, id: string) {
    // Set this true, it means it's marked
    task.hubMarkBoolean = true;
    this.taskService.markTask(task, id).subscribe(
      data => {},
      err => {
        this.errorMessage = "Keine Netzwerkverbindung";
        console.log(this.errorMessage);
      }
    );
  }

  /**
   * Refresh the list
   */
  doRefresh(event) {
    console.log("Refresh the list");

    // Get all tasks
    this.findAllTasks();

    setTimeout(() => {
      console.log("List refreshed");
      event.target.complete();
    }, 2000);
  }

  detail(user: User) {
    this.router.navigate(["/detail", user.id]);
    localStorage.setItem("detailUser", JSON.stringify(user));
  }
}
