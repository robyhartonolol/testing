import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Location } from "../model/location";

let API_URL = "http://localhost:8080/api/location/";

@Injectable({
  providedIn: "root"
})
export class LocationService {
  constructor(private http: HttpClient) {}

  findAllLocations(): Observable<any> {
    return this.http.get(API_URL, {
      headers: { "Content-Type": "application/json; charset=UTF-8" }
    });
  }

  findByIdLocation(id: string): Observable<any> {
    return this.http.get(API_URL + id, {
      headers: { "Content-Type": "application/json; charset=UTF-8" }
    });
  }
}
