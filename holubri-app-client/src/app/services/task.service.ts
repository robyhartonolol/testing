import { Task } from "./../model/task";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

let API_URL = "http://localhost:8080/api/task/";

@Injectable({
  providedIn: "root"
})
export class TaskService {
  constructor(private http: HttpClient) {}

  findAllTasks(): Observable<any> {
    return this.http.get(API_URL, {
      headers: { "Content-Type": "application/json; charset=UTF-8" }
    });
  }

  markTask(task: Task, id: string): Observable<any> {
    return this.http.put(API_URL + id, JSON.stringify(task), {
      headers: { "Content-Type": "application/json; charset=UTF-8" }
    });
  }
}
