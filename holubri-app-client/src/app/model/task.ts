import { User } from './user';
import { Location } from './location';
import { Station } from './station';
import { TaskType } from './tasktype';
import { Patient } from './patient';


export class Task{
    id:string; 
    taskType:TaskType;
    startLocation:Location;
    endLocation:Location;
    station:Station;
    movementProfileString:string;
    owner:User[];
    patient: Patient;
    planDateTime:string;
    beginDateTime:string;
    endDateTime:string;
    behandlerName:string; 
    hubListBoolean:boolean;
    hubMarkBoolean:boolean;
    hubCompleted: boolean;
    elevatorString:string;
    bridgeCourtString:string;
}