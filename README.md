# Viktor App

Viktor (Take and Bring service) is an App to support and optimize the workflow of HUB-Workers in BDH Clinic in Braunfels

## How To

### 1. Start Application Server 
1.  Import holubri-api-data folder in any spring IDE (STS, Intelijj) as a Maven Project.
2.  Start a XAMPP Control Panel (Checklist Apache and MySQL).
3.  Start HolubriApplication.java as a SpringBoot Application 
4.  Open the app through browser localhost:8080

### 2. Start Mobile Client 
5.  Open holubri-api-client folder in any IDE 
6.  Type ionic serve on terminal 
7.  Open the app through browser localhost:8100

# Developer
For the developer please use the following language in this project. 
* Wiki, Documentation, Issues : Deutsch
* Source code : English